fn calculate_difference(numbers: (u64, u64)) -> u64 {
    if numbers.0 > numbers.1 {
        numbers.0 - numbers.1
    } else {
        numbers.1 - numbers.0
    }
}

fn read_numbers(line: String) -> (u64, u64) {
    let mut slices = line.trim().split_ascii_whitespace();
    (
        slices.next().unwrap().parse::<u64>().unwrap(),
        slices.next().unwrap().parse::<u64>().unwrap(),
    )
}
fn main() {
    loop {
        let mut input = String::new();

        match std::io::stdin().read_line(&mut input) {
            Ok(n) => {
                if n == 0 {
                    break;
                }
                println!("{}", calculate_difference(read_numbers(input)));
            }
            Err(error) => println!("error: {}", error),
        }
    }
}
